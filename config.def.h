static const char *background_color = "#1c1c1c";
static const char *border_color = "#af875f";
static const char *font_color = "#dfdfaf";
static const char *font_pattern = "monospace:size=14";
static const unsigned line_spacing = 5;
static const unsigned int padding = 18;
static const int use_primary_monitor = 1;

static const unsigned int width = 288;
static const unsigned int border_size = 3;
static const unsigned int pos_x = 48;
static const unsigned int pos_y = 60;

enum corners { TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT };
enum corners corner = BOTTOM_RIGHT;

static const unsigned int duration = 5; /* in seconds */

#define DISMISS_BUTTON Button1
#define ACTION_BUTTON Button3
